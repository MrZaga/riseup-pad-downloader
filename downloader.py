#!/usr/bin/env python3
import datetime
import os

link_array = []

#create pad directory if it doesn't exist
if not os.path.exists('pads'):
    os.makedirs('pads')

#pad download & save function
def download_pad(padlink):
    filename_addition = datetime.datetime.today().strftime('%Y-%m-%d-%H:%M:%S')
    filedest = 'pads/'

    wget_string = 'wget {}/export/txt'.format(padlink)
    os.system(wget_string)
    move_string = 'mv txt {}{}'.format(filedest, filename_addition)
    os.system(move_string)



#get links from etherpad.conf, ignore comments
with open('etherpad.conf') as conffile:
    for line in conffile:
        li=line.strip()
        if not li.startswith("#"):
            link_array.append(line)

#Download pads
for padlink in link_array:
    download_pad(padlink)

#clear link_array and exit
link_array.clear()