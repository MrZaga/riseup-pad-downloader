<h1>Download Etherpads</h1>
This bit of software can be used to automatically download Etherpads, RiseUp Pads etc.
<h2> Configuration </h2>
All the pad links are in etherpad.conf, each link on a new line.<br>
If you want to automatically download the pads, create a new cronjob and run the script from there.